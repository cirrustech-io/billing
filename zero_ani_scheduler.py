#!/usr/bin/env python

from billing_helper import fetch_data, get_csv_writer, write_rows_to_csv, send_mailer, close_csv
from billing_config import output_pbx_dir, pbx_receiver, file_path
from datetime import datetime, date, timedelta
from log_generator import log_writer, syslog_writer

app_name = 'zero_ani'
log_file = file_path + 'logs/'+app_name+'.log'
logger = log_writer(log_file, app_name)

def get_pbx(start_date, end_date, src_id):
    pbx_data = fetch_data("SELECT start_time, ani, call_duration_fractional, clean_number, call_source_regid FROM new_cdr WHERE start_time>='%s' AND start_time<'%s' AND call_source_regid = '%s' AND (ani ~ '^0[0]' OR ani='');" % (start_date, end_date, src_id));
   
    msg = "Fetch Data for : %s - %s" % (start_date, pbx_data)
    logger.info(msg)
    #syslog_writer(app_name, "INFO", msg)
    if(pbx_data):

        out_month = datetime.strptime(start_date, "%Y-%m-%d").strftime("%d_%b_%Y")
        out_month_text = datetime.strptime(start_date, "%Y-%m-%d").strftime("%B %d - %Y")
        out_file = output_pbx_dir+'hdfc_pbx_zero_ani_'+out_month+'.csv'
        out_header = ['Call Date','ANI','Call Duration', 'Called Number','Originating Carrier']
        pbx_writer, out_fd = get_csv_writer(out_file, out_header)
        write_rows_to_csv(pbx_writer, pbx_data)
        close_csv(out_fd)

        subject = "Zero Ani Notification"
        content = "Attached the Zero Ani Report for %s" % (out_month_text)

        send_mailer(pbx_receiver, subject, content, out_file, 'text/csv')
        msg = "Send zero ani mail to %s" % pbx_receiver
        logger.info(msg)
        syslog_writer(app_name, "INFO", msg)


if __name__ == '__main__':

    import sys
    date_format = "%Y-%m-%d"

    if len(sys.argv) == 1:
        src_id = "HDFC_PBX"
        start_date = datetime.strftime((date.today() - timedelta(days=1)), date_format)
        end_date = datetime.strftime(date.today(), date_format)
    else:
        start_date = sys.argv[1]
        end_date = sys.argv[2]
        src_id = sys.argv[3]

    msg = "%s : Generating %s report for : %s" % (datetime.now(), src_id, start_date)
    logger.info(msg)
    syslog_writer(app_name, "INFO", msg)
   
    
    get_pbx(start_date, end_date, src_id)

