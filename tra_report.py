#!/usr/bin/env python

from billing_helper import fetch_data, get_csv_writer, write_rows_to_csv, write_to_csv, send_mailer, close_csv
from billing_config import output_tra_dir, file_path, O2_rate_dict, number_range_lookup
from datetime import datetime, date, timedelta
from log_generator import log_writer, syslog_writer
from tra_infonas_termination import generate_infonas_report

app_name = 'tra_report'
log_file = file_path + 'logs/'+app_name+'.log'
logger = log_writer(log_file, app_name)

def get_tra(start_date, end_date, src_id):
    tra_data = fetch_data("SELECT start_time, ani, call_duration_fractional, call_duration_fractional/60, clean_number, call_source_regid FROM new_cdr WHERE (call_source_regid = 'O2_CETIN_NNI_1' OR call_source_regid = 'O2_CETIN_NNI_2') AND call_source_uport = 90 AND call_duration_fractional > 0.0 AND start_time>='%s' AND start_time<'%s' AND ani NOT LIKE '%s';" % (start_date, end_date, '00973%'));
   
    msg = "Fetch Data for : %s - %s" % (start_date, tra_data)
    logger.info(msg)
    #syslog_writer(app_name, "INFO", msg)
    if(tra_data):

        from_month = datetime.strptime(start_date, "%Y-%m-%d").strftime("%d_%b_%Y")
        to_month = datetime.strptime(end_date, "%Y-%m-%d").strftime("%d_%b_%Y")
        out_month_text = datetime.strptime(start_date, "%Y-%m-%d").strftime("%B %d - %Y")
        out_file = output_tra_dir+'tra_report_'+from_month+'_to_'+to_month+'.csv'
        out_header = ['Call Date','ANI','Call Duration(sec)', 'Call Duration(min)', 'Called Number','Originating Carrier']
        tra_writer, out_fd = get_csv_writer(out_file, out_header)
        write_rows_to_csv(tra_writer, tra_data)
        close_csv(out_fd)
        src_dct = O2_rate_dict.copy()
        tot_charge = 0.0
        tot_minut = 0.0
        for tra in tra_data:
            src_carrier = number_range_lookup(int(tra[4]))
            minut = float(tra[3])
            charge = minut * float(src_dct[src_carrier]['rate'])
            src_dct[src_carrier]['minute'] += minut
            src_dct[src_carrier]['charge'] += charge
            tot_charge += charge
            tot_minut += minut

        print "TOTAL MINUTE : ",tot_minut
        print "TOTAL CHARGE : ",tot_charge

        ### Create O2 Traffic - Vendor based Report
        o2_src_file = output_tra_dir+'tra_report_carrier_based_'+from_month+'_to_'+to_month+'.csv'
        o2_src_headers = ['Source','Minutes','Rate (BHD)','Charges (BHD)']
        o2_src_writer, o2_src_fd = get_csv_writer(o2_src_file, o2_src_headers)

        src_charge = 0.0
        src_min = 0.0
        src_call = 0

        for key,value in src_dct.items():
            if(src_dct[key]['minute']):
                print key, value
                o2_src_out = [key, src_dct[key]['minute'], str(src_dct[key]['rate']), str(src_dct[key]['charge'])]
                write_to_csv(o2_src_writer, o2_src_out)
                src_charge += src_dct[key]['charge']
                src_min += src_dct[key]['minute']

        write_to_csv(o2_src_writer, ['Total','',src_call,round(src_min,2),'',str(round(src_charge, 2))])
        close_csv(o2_src_fd)
        logger.info("Completed writing into %s" % (o2_src_file))
        syslog_writer(app_name, "INFO", "Completed writing into %s" % (o2_src_file))

        msg = "Fetch Data Complete for : %s" % app_name
        logger.info(msg)
        syslog_writer(app_name, "INFO", msg)

def get_inbound_national_calls(start_date, end_date):

    in_query = "select start_time, call_source_regid, ani, call_source_uport, clean_number, call_duration_fractional from new_cdr where start_time >= '%s' and start_time < '%s' and call_duration_fractional> 0.0 AND ((clean_number LIKE '%s') OR (clean_number LIKE '%s')) AND ((call_source_regid = '2CONNECT_AS5400A' AND call_source_uport IN (10,60)) OR (call_source_regid = '2CONNECT_AS5350B' AND call_source_uport IN (50,60)) OR (call_source_regid = '2CONNECT_AS5850B' AND call_source_uport IN (10,60)) );" % (start_date, end_date, '973165%', '9738008%')
    nat_data = fetch_data(in_query)

    logger.info(in_query)
    #msg = "Fetch Data for : %s - %s" % (start_date, nat_data)
    #logger.info(msg)

    from_month = datetime.strptime(start_date, "%Y-%m-%d").strftime("%d_%b_%Y")
    to_month = datetime.strptime(end_date, "%Y-%m-%d").strftime("%d_%b_%Y")
    out_month_text = datetime.strptime(start_date, "%Y-%m-%d").strftime("%B %d - %Y")
    out_file = output_tra_dir+'infonas_termination_db_report_'+from_month+'_to_'+to_month+'.csv'
    out_header = ['Call Date','Source RegID', 'ANI', 'Source Port', 'Called Number','Call Seconds']
    nat_writer, out_fd = get_csv_writer(out_file, out_header)
    write_rows_to_csv(nat_writer, nat_data)
    close_csv(out_fd)
    generate_infonas_report(out_file)

if __name__ == '__main__':

    import sys
    date_format = "%Y-%m-%d"

    if len(sys.argv) == 1:
        src_id = "O2"
        start_date = datetime.strftime((date.today() - timedelta(days=1)), date_format)
        end_date = datetime.strftime(date.today(), date_format)
    else:
        start_date = sys.argv[1]
        end_date = sys.argv[2]
        src_id = sys.argv[3]

    msg = "%s : Generating %s report for : %s" % (datetime.now(), src_id, start_date)
    logger.info(msg)
    syslog_writer(app_name, "INFO", msg)
  
    if src_id == "domestic":
        get_inbound_national_calls(start_date, end_date)
    else: 
        get_tra(start_date, end_date, src_id)

