<?php
/*
 * version 4
 * In this version omitted the calls that are being mis-counted as calls going to Russia
 * This is done by only counting international calls going through
 * Certain call_dest_regid
 * 
 * version 3
 * In this version added the capability to enter the start and end date from 
 * HTML form instead of altering the code everytime
 * 
 * version 2
 * In this version - added calculations of the following parameters:
 * Outgoing International failure D.11
 * protocol failure D.13
 * 
 */

include_once('DBAccessor.php');
$start_date = date('d-M-Y');
$end_date = date('d-M-Y');

$_POST=array("start_date"=>"01-Apr-2016","end_date"=>"30-Jun-2016");

if (isset($_POST['start_date']) && isset($_POST['end_date'])){
        echo $_POST['start_date'];
	if (!preg_match('/^\d{2}[\-]\w{3}[\-]\d{4}$/',$_POST['start_date']) || 
			!preg_match('/^\d{2}[\-]\w{3}[\-]\d{4}$/',$_POST['end_date'])) {
		echo "Dates must be in  dd-mmm-yyyy format";
	}
	else {
		echo "Dates are correct!<br>";
		
		$DB = startDB();

		echo "<br>Extracting Countries..<br>";
		
		
		$sql = "SELECT country_code,country_name
			from country_temp
			order by country_code ASC";
		
		$result = pg_query($sql) or die(pg_error());
		
		
		if(pg_num_rows($result) <= 0){
			echo "<br>No Countries found<br>";
			exit(0);
		} else {
		
			$COUNTRIES = array();
			while($line=pg_fetch_array($result, null, PGSQL_ASSOC)){
				$COUNTRIES[] = $line;
			}
		
			
			//print_r($COUNTRIES);
			//exit(0);
			
			echo "<br>Creating Country Buckets<br>";
			$B_COUNTRIES = CreateCountryBuckets(5,$COUNTRIES);
		
		}
		
		echo "<br>Extracting CDRs..<br>";
		
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		
		$sql = "SELECT ani,clean_number,call_duration_int, call_duration,call_error_int,isdn_cause_code, terminator_ip, call_dest_regid, call_dest_uport 
			from new_cdr
			where start_time > '$start_date 00:00:00' and start_time <= '$end_date 23:59:59'";
		
		$result = pg_query($sql);
		
		if(pg_num_rows($result) <= 0){
			echo "<br>No CDRs found<br>";
			exit(0);
		}
		$filename = "$start_date to $end_date Output.csv";
                echo $filename;
		$genius = fopen($filename,'w');
		
		
		//bahrain mobile
		$mobile_count = 0;
		$success_mobile_count = 0;
		$mobile_isdn = array(); $mobile_isdn['empty'] = 0;
		$mobile_errorint = array();
		
		//bahrain fixed
		$fixed_count = 0;
		$success_fixed_count = 0;
		$fixed_isdn = array(); $fixed_isdn['empty'] = 0;
		$fixed_errorint = array();
		
		
		//bahrain emergency
		$eme_count = 0;
		$success_eme_count = 0;
		$eme_isdn = array(); $eme_isdn['empty'] = 0;
		$eme_errorint = array();
		
		//bahrain directory assist
		$dir_count = 0;
		$success_dir_count = 0;
		$dir_isdn = array(); $dir_isdn['empty'] = 0;
		$dir_errorint = array();
		
		
		//intl calls
		$intl_count = 0;
		$success_intl_count = 0;
		$intl_isdn = array(); $intl_isdn['empty'] = 0;
		$intl_errorint = array();
		$asr = array();
		
		//D.11 - Unsuccessful calls: Positive indication of failure from outgoing international exchange
		$outgoing_fail = 0;
		
		//D.12 - Successful calls with defects
		// --> cannot be measured becausewe cannot check calls at SBC (Session Border Control)
		
		//D.13 - Unsuccessful calls due to signaling system failure
		$protocol_fail = 0;
		
		
		$total = pg_num_rows($result);
		$current = 0;
		
		echo "<br>Messing with CDRs...<br>";
		
		while($line = pg_fetch_array($result,null,PGSQL_ASSOC)){
			//$current ++;
			//echo "Progress ({$current}/$total) " . round((($current*100)/$total),2) . "% \r";
			//echo "$current out of $total done<br>";
			
			$ani = $line['ani'];
			$clean_number = $line['clean_number'];
			$call_duration_int = $line['call_duration_int'];
			$call_duration = $line['call_duration'];
			$call_error_int = $line['call_error_int'];
			$isdn_cause_code = $line['isdn_cause_code'];
			$terminator_ip = $line['terminator_ip'];
			$call_dest_regid = $line['call_dest_regid'];
			$call_dest_uport = $line ['call_dest_uport'];
		
			//Calls going to Bahrain Mobile
			if(preg_match("/^3\d{7}$/",$clean_number) || preg_match("/^9733\d{7}$/",$clean_number)){
		
				//total
				$mobile_count++;
		
				//success
				if($call_duration_int > 0){ $success_mobile_count++; }
		
				//summarize - isdn_cause_code
				if($isdn_cause_code == '') { $mobile_isdn['empty'] += 1; }
				elseif(array_key_exists($isdn_cause_code,$mobile_isdn)){ $mobile_isdn[$isdn_cause_code] += 1; }
				else{ $mobile_isdn[$isdn_cause_code] = 1; }
		
		
				//summarize call error int
				if(array_key_exists($call_error_int,$mobile_errorint)){ $mobile_errorint[$call_error_int] += 1; }
				else{ $mobile_errorint[$call_error_int] = 1; }
		
			 }
		
			 //Calls going to Bahrain Fixed
			  if(preg_match("/^1\d{7}$/",$clean_number) || preg_match("/^9731\d{7}$/",$clean_number)){
		
				//total
				$fixed_count++;
		
				//success
				if($call_duration_int > 0){ $success_fixed_count++; }
		
				//summarize - isdn_cause_code
				if($isdn_cause_code == '') { $fixed_isdn['empty'] += 1; }
				elseif(array_key_exists($isdn_cause_code,$fixed_isdn)){ $fixed_isdn[$isdn_cause_code] += 1; }
				else{ $fixed_isdn[$isdn_cause_code] = 1; }
			   																			                 //summarize call error int
				if(array_key_exists($call_error_int,$fixed_errorint)){ $fixed_errorint[$call_error_int] += 1; }							              else{ $fixed_errorint[$call_error_int] = 1; }
																							}
		
			//Calls going to Bahrain Emergency
			if(preg_match("/^199$/",$clean_number) || preg_match("/^999$/",$clean_number)){
		
				//total
				$eme_count++;
		
				//success
				if($call_duration_int > 0){ $success_eme_count++; }
		
				//summarize - isdn_cause_code
				if($isdn_cause_code == '') { $eme_isdn['empty'] += 1; }
				elseif(array_key_exists($isdn_cause_code,$eme_isdn)){ $eme_isdn[$isdn_cause_code] += 1; }
				else{ $eme_isdn[$isdn_cause_code] = 1; }
		
																					                     //summarize call error int
				if(array_key_exists($call_error_int,$eme_errorint)){ $eme_errorint[$call_error_int] += 1; }
				else{ $eme_errorint[$call_error_int] = 1; }
		
			}
		
			//Calls going to Directory Assist
			if(preg_match("/^188$/",$clean_number) || preg_match("/^181$/",$clean_number)){
		
					//total
					$dir_count++;
		
					//success
					if($call_duration_int > 0){ $success_dir_count++; }
		
					//summarize - isdn_cause_code
					if($isdn_cause_code == '') { $dir_isdn['empty'] += 1; }
					elseif(array_key_exists($isdn_cause_code,$dir_isdn)){ $dir_isdn[$isdn_cause_code] += 1; }
					else{ $dir_isdn[$isdn_cause_code] = 1; }
		
					//summarize call error int
					if(array_key_exists($call_error_int,$dir_errorint)){ $dir_errorint[$call_error_int] += 1; }
					else{ $dir_errorint[$call_error_int] = 1; }
		
			}
		
		
		
			//International Calls
			if(!(preg_match("/^973/",$clean_number)) && (preg_match("/^\d{9,}$/",$clean_number)
			&& !(preg_match("/^77/",$clean_number)))){
				//Make sure it is international call:
				if (  ($call_dest_regid == 'BHARTI_VoIP' and $call_dest_uport == 80) 
				    || ($call_dest_regid == 'UK_AS5400A' and $call_dest_uport == 80) 
				    || ($call_dest_regid == 'UK_AS5350B' and $call_dest_uport == 80) 
				    || ($call_dest_regid == 'UK_AS5400A' and $call_dest_uport == 81) 
				    || ($call_dest_regid == 'VSNL_7'     and $call_dest_uport == 80)
				    || ($call_dest_regid == 'VSNL_10'    and $call_dest_uport == 80)
				    || ($call_dest_regid == '2CONNECT_AS5400A' and $call_dest_uport == 10)
				    || ($call_dest_regid == '2CONNECT_AS5400A' and $call_dest_uport == 51)
				    || ($call_dest_regid == '2CONNECT_AS5350B' and $call_dest_uport == 50)
				    || ($call_dest_regid == 'UK_AS5400A' and $call_dest_uport == 10) 
				    || ($call_dest_regid == 'UK_AS5350B' and $call_dest_uport == 10) 
				    || ($call_dest_regid == 'BHARTI_VoIP' and $call_dest_uport == 10) 
				    || ($call_dest_regid == 'UK_AS5400A' and $call_dest_uport == 11)
				    || ($call_dest_regid == 'UK_AS5400A' and $call_dest_uport == 91) 
				    || ($call_dest_regid == 'UK_AS5350B' and $call_dest_uport == 91)
				    || ($call_dest_regid == 'UK_AS5400A' and $call_dest_uport == 90) 
				    || ($call_dest_regid == 'UK_AS5350B' and $call_dest_uport == 90)) 
				    {
						//total
						$intl_count++;
			
						//success
						if($call_duration_int > 0){
							$success_intl_count++;
						}
			
						//summarize - isdn_cause_code
						if($isdn_cause_code == '') { 
							$intl_isdn['empty'] += 1; 
							//international call, no ISDN cause code, destination IP = 0.0.0.0
							//---> Outgoing International failure..
							if (preg_match("/^0.0.0.0$/",$terminator_ip ))
								$outgoing_fail++;
						}
						elseif(array_key_exists($isdn_cause_code,$intl_isdn)){ $intl_isdn[$isdn_cause_code] += 1; }
						else{ $intl_isdn[$isdn_cause_code] = 1; }
			
						//summarize call error int
						if(array_key_exists($call_error_int,$intl_errorint)){ $intl_errorint[$call_error_int] += 1; }
						else{ $intl_errorint[$call_error_int] = 1; }
			
			
			
						//ASR Calculation
			
						//isolate country
						$check = substr($clean_number,0,5);
						if(array_key_exists($check,$B_COUNTRIES)){ $country = $B_COUNTRIES[$check]; }
						else {$country = 'Unknown';}
			
						if(array_key_exists($country,$asr)){
							$asr[$country]['total'] += 1;
							if($call_duration_int > 0){ $asr[$country]['ans'] += 1;}
						} else {
							$asr[$country]['total'] = 1;
							if($call_duration_int > 0){ $asr[$country]['ans'] = 1;}
							else { $asr[$country]['ans'] = 0;}
						}
				    }
			}
		
		
		}
		
		
		echo "<br>Writing Output...<br>";
		
		//Write Output
		
		fwrite($genius,"Mobile Calls\r\n");
		fwrite($genius,"Total Calls=$mobile_count\r\n");
		fwrite($genius,"Success Calls=$success_mobile_count\r\n");
		fwrite($genius,"ISDN Cause Code Info\r\n");
		fwrite($genius,"Code,Call Count,\r\n");
		foreach($mobile_isdn as $key=>$val){
			fwrite($genius,"$key,$val,\r\n");
		}
		fwrite($genius,",,Error Int Info\r\n");
		fwrite($genius,",,Code,Call Count,\r\n");
		foreach($mobile_errorint as $key=>$val){
			fwrite($genius,",,$key,$val,\r\n");
		}
		
		fwrite($genius,"\r\n");
		fwrite($genius,"\r\n");
		
		fwrite($genius,",,,,Fixed Calls\r\n");
		fwrite($genius,",,,,Total Calls=$fixed_count\r\n");
		fwrite($genius,",,,,Success Calls=$success_fixed_count\r\n");
		fwrite($genius,",,,,ISDN Cause Code Info\r\n");
		fwrite($genius,",,,,Code,Call Count,\r\n");
		foreach($fixed_isdn as $key=>$val){
			fwrite($genius,",,,,$key,$val,\r\n");
		}
		fwrite($genius,",,,,,,Error Int Info\r\n");
		fwrite($genius,",,,,,,Code,Call Count,\r\n");
		foreach($fixed_errorint as $key=>$val){
			fwrite($genius,",,,,,,$key,$val,\r\n");
		}
		
		fwrite($genius,"\r\n");
		fwrite($genius,"\r\n");
		
		
		fwrite($genius,",,,,,,,,Emergency Calls\r\n");
		fwrite($genius,",,,,,,,,Total Calls=$eme_count\r\n");
		fwrite($genius,",,,,,,,,Success Calls=$success_eme_count\r\n");
		fwrite($genius,",,,,,,,,ISDN Cause Code Info\r\n");
		fwrite($genius,",,,,,,,,Code,Call Count,\r\n");
		foreach($eme_isdn as $key=>$val){
			fwrite($genius,",,,,,,,,$key,$val,\r\n");
		}
		fwrite($genius,",,,,,,,,,,Error Int Info\r\n");
		fwrite($genius,",,,,,,,,,,Code,Call Count,\r\n");
		foreach($eme_errorint as $key=>$val){
			fwrite($genius,",,,,,,,,,,$key,$val,\r\n");
		}
		
		fwrite($genius,"\r\n");
		fwrite($genius,"\r\n");
		
		fwrite($genius,",,,,,,,,,,,,Directory Assist Calls\r\n");
		fwrite($genius,",,,,,,,,,,,,Total Calls=$dir_count\r\n");
		fwrite($genius,",,,,,,,,,,,,Success Calls=$success_dir_count\r\n");
		fwrite($genius,",,,,,,,,,,,,ISDN Cause Code Info\r\n");
		fwrite($genius,",,,,,,,,,,,,Code,Call Count,\r\n");
		foreach($dir_isdn as $key=>$val){
			fwrite($genius,",,,,,,,,,,,,$key,$val,\r\n");
		}
		fwrite($genius,",,,,,,,,,,,,,,Error Int Info\r\n");
		fwrite($genius,",,,,,,,,,,,,,,Code,Call Count,\r\n");
		foreach($dir_errorint as $key=>$val){
			fwrite($genius,",,,,,,,,,,,,,,$key,$val,\r\n");
		}
		
		fwrite($genius,"\r\n");
		fwrite($genius,"\r\n");
		
		
		fwrite($genius,",,,,,,,,,,,,,,,,International Calls\r\n");
		fwrite($genius,",,,,,,,,,,,,,,,,Total Calls=$intl_count\r\n");
		fwrite($genius,",,,,,,,,,,,,,,,,Success Calls=$success_intl_count\r\n");
		fwrite($genius,",,,,,,,,,,,,,,,,Code,Call Count,\r\n");
		fwrite($genius,",,,,,,,,,,,,,,,,ISDN Cause Code Info\r\n");
		foreach($intl_isdn as $key=>$val){
			fwrite($genius,",,,,,,,,,,,,,,,,$key,$val,\r\n");
		}
		//Calculate protocol failure -- D.13
		fwrite($genius,",,,,,,,,,,,,,,,,,,Error Int Info\r\n");
		fwrite($genius,",,,,,,,,,,,,,,,,,,Code,Call Count,\r\n");
		foreach($intl_errorint as $key=>$val){
			fwrite($genius,",,,,,,,,,,,,,,,,,,$key,$val,\r\n");
			if ($key == 1004 || $key == 1005 || $key == 1006 || $key == 1008 || $key == 1009
				|| $key == 1022 || $key >= 5000)
				$protocol_fail += $val;
		}
		fwrite($genius, "\r\n");
		fwrite($genius,",,,,,,,,,,,,,,,,,,,,D.13 - Protocol Failure Call count,$protocol_fail ");
		
		fwrite($genius, "\r\n");
		fwrite($genius,",,,,,,,,,,,,,,,,,,,,D.11 - International Failure Call count,$outgoing_fail");
		
		fwrite($genius,"\r\n");
		fwrite($genius,"\r\n");
		
		fwrite($genius,",,,,,,,,,,,,,,,,,,,,,,ASR Info International Calls\r\n");
		fwrite($genius,",,,,,,,,,,,,,,,,,,,,,,Country,Total,Answered,\r\n");
		foreach($asr as $country=>$val){
			$tot = $val['total'];
			$ans = $val['ans'];
			fwrite($genius,",,,,,,,,,,,,,,,,,,,,,,$country,$tot,$ans\r\n");
		}
		
		fwrite($genius,"\r\n");
		fwrite($genius,"\r\n");
		
		
		fclose($genius);
		
		echo "<br>Done<br />";
		echo "Output is stored in the file: $filename <br /> <br />";


	}//if dates entered are in correct format
		
}// if text fields are set

function CreateCountryBuckets($bucket_length,$COUNTRIES){
		
				//output array
				$B_COUNTRIES = array();
		
				//loop through countries and create bucket
				foreach($COUNTRIES as $c){
		
					//extract bucket length number of digits from the dialcode
					$trimmed = substr($c['country_code'],0,$bucket_length);
		
					//if trimmed dialcode length is less than bucket length, manually create buckets for missing digits
					if(strlen($trimmed) < $bucket_length){
		
						$diff = $bucket_length - strlen($trimmed);
						$start =  $trimmed * pow(10,$diff);
						$end = $start + pow(10,$diff) - 1;
		
		
						for($i = $start; $i <= $end ; $i++){
							$B_COUNTRIES[$i] = $c['country_name'];
						}
					} else {
						$B_COUNTRIES[$trimmed]= $c['country_name'];
					}
				}
		
				return $B_COUNTRIES;
		}//function create country buckets


?>
<!--
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Observations on calls</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
</head>

<body>
<form action= <?php echo $_SERVER['PHP_SELF']; ?> method="post" name="form1" id="form1">

<table>
<tr><td>Start Date:</td><td><input type="text" name="start_date" value="<?php echo $start_date ?>" /></td></tr>
<tr><td>End Date:</td><td><input type="text" name="end_date" value="<?php echo $end_date ?>" /></td></tr>	 
<tr><td><input type="submit" name="Submit" value="Run Script"/></td></tr>    
</table>
</form>
</body>
</html>

-->
