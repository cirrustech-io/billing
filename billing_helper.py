
import psycopg2
import csv
from datetime import timedelta, datetime

import smtplib
# For guessing MIME type
import mimetypes
# Import the email modules we'll need
import email
import email.mime.application
from billing_config import smtp_host, smtp_port, sender, db_name, db_user, db_host, db_password
import math

def fetch_data(query):

    try:
        conn = psycopg2.connect("dbname='%s' user='%s' host='%s' password='%s'" % (db_name, db_user, db_host, db_password))
        cur = conn.cursor()
        cur.execute(query)
        rows = cur.fetchall()
        return rows
    except:
        return "Unable to connect to the database"

def get_csv_writer(output_file, output_headers):

    out_fd = open(output_file, 'wb')
    out_writer = csv.writer(out_fd)
    out_writer.writerow(output_headers)
    return out_writer, out_fd

def write_to_csv(out_writer, record):
    out_writer.writerow(record)

def write_rows_to_csv(out_writer, record):
    out_writer.writerows(record)

def close_csv(out_fd):
    out_fd.close()

def get_duration(sec):
    return str(timedelta(seconds = sec))

def truncate_digit(invalue, digits):
    return int(invalue * math.pow(10,digits))/math.pow(10,digits)

def convert_dur_to_sec(timestr):
    seconds= 0
    for part in timestr.split(':'):
        seconds= seconds*60 + int(part)
    return seconds

def convert_sec_to_min(sec):
    return float(sec)/60

def send_mailer(receiver, subject, content, attachment, attach_type):

    # Create a text/plain message
    msg = email.mime.Multipart.MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = receiver[-1]

    # The main body is just another attachment
    body = email.mime.Text.MIMEText(content)
    msg.attach(body)

    if (attachment):
        add_attachment(attachment, attach_type, msg)

    # Send mail. I use the default port 25, but I authenticate. 
    s = smtplib.SMTP(smtp_host, smtp_port)
    s.sendmail(sender, receiver, msg.as_string())
    s.quit()

def add_attachment(filename, attach_type, msg):
    # attachment

    fp=open(filename,'rb')
    att = email.mime.application.MIMEApplication(fp.read(),_subtype=attach_type)
    fp.close()
    
    att.add_header('Content-Disposition','attachment',filename=filename.split("/")[-1].strip())
    msg.attach(att)

if __name__ == '__main__':
   
    import sys
    timestr = sys.argv[1] 
    print convert_dur_to_sec(timestr)



