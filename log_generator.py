import logging
import syslog


def log_writer(filename, appname):

    logger = logging.getLogger(appname)
    hdlr = logging.FileHandler(filename)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr) 
    logger.setLevel(logging.DEBUG)
    return logger

def syslog_writer(appname, levelname, message):
    ## Write logs into sys log
    
    syslog.openlog(logoption=syslog.LOG_PID, facility=syslog.LOG_MAIL)
    syslog.syslog('%s - %s - %s' % (appname, levelname, message))


