#
# Small script to show PostgreSQL and Pyscopg together
#

import csv
import json
from billing_helper import get_csv_writer, write_to_csv, close_csv, convert_dur_to_sec, convert_sec_to_min, truncate_digit

def get_infonas_rate(inv_filename, rate_filename, out_file, markup=None):
   with open(inv_filename, 'rb') as inv_file:
       inv_reader = csv.reader(inv_file, delimiter=',', quotechar='|')

       out_header = ['Country', 'Dial Code', 'Duration', 'Minutes', 'Infonas Rate(BHD)', 'Cost(BHD)-Infonas', 'Cost(BHD)-Other Provider']
       inv_writer, out_fd = get_csv_writer(out_file, out_header)

       tot_charge = 0.0
       tot_pro_charge = 0.0
       tot_min = 0.0
       tot_calls = 0
       for row in inv_reader:
           with open(rate_filename, 'rb') as rate_file:
               rate_reader = csv.reader(rate_file, delimiter=',', quotechar='|')

               for rate in rate_reader:
                   if row[3].strip() == rate[0].strip():
                       sec = convert_dur_to_sec(row[4])
                       minut = round(convert_sec_to_min(sec), 3)

                       new_rate = truncate_digit(float(rate[2]), 4)
                       print "OLD", new_rate
                       if markup:
                           new_rate = ((float(markup)/100.0) * float(rate[2])) + float(rate[2])
                           new_rate = truncate_digit(new_rate, 4)

                       charge = round((float(new_rate) * float(minut)), 3)

                       pro_charge = float(row[5].replace('Bahraini Dinar','').strip())
                       #calls = int(row[4])
                       print "NEW", new_rate 
                       record = [row[3].strip(), rate[1], row[4], minut, new_rate, charge, pro_charge]
                       tot_charge += charge
                       tot_pro_charge += pro_charge
                       #tot_calls += calls
                       tot_min += minut
                       print record
                       write_to_csv(inv_writer, record)
                       #close_csv(out_fd)
       print tot_charge
       ### Write Total Charge to the Detailed Report
       write_to_csv(inv_writer, ['Total', '', '', '%.2f' % tot_min, '', 'BHD %.2f' % tot_charge, 'BHD %.2f' % tot_pro_charge])
       close_csv(out_fd)

def create_infonas_rate(inf_file, sup_file, out_file):


    with open(inf_file, 'rb') as inf_fle:
       inf_reader = csv.reader(inf_fle, delimiter=',', quotechar='|')

       out_header = ['Country', 'Dial Code', 'Rate(BHD)']
       inf_writer, out_fd = get_csv_writer(out_file, out_header)

       for inf in inf_reader:
           rate_dct = {'country':inf[0], 'code':inf[1], 'rate':inf[2]}
           with open(sup_file, 'rb') as sup_fle:
               sup_reader = csv.reader(sup_fle, delimiter=',', quotechar='|')
               for sup in sup_reader:
                   if (inf[0].lower().strip() in sup[0].lower().strip()):
                       if float(sup[2]) > float(rate_dct['rate']):
                           rate_dct['rate'] = float(sup[2])

           if rate_dct['rate'] > 0.0:
               print rate_dct
               write_to_csv(inf_writer, [rate_dct['country'], rate_dct['code'], rate_dct['rate']])


    

if __name__ == '__main__':
    
    import sys
    inv_fle = sys.argv[1]
    rate_fle = sys.argv[2]
    inv_out_file = sys.argv[3]
    try:
        markup = sys.argv[4]
    except:
        markup=None
    get_infonas_rate(inv_fle, rate_fle, inv_out_file, markup)
    #create_infonas_rate(inv_fle, rate_fle, inv_out_file)

        
