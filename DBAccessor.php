<?php


include('includes/functions.inc.php');

function startDB(){

# PHP ADODB document - made with PHAkt
	# FileName="Connection_php_adodb.htm"
	# Type="ADODB"
	# HTTP="true"
	# DBTYPE="postgres7"

	$MM_DB_HOSTNAME = 'pacer.2connectbahrain.com';
	$MM_DB_DATABASE = 'postgres8:billing';
	$MM_DB_DBTYPE   = preg_replace('/:.*$/', '', $MM_DB_DATABASE);
	$MM_DB_DATABASE = preg_replace('/^[^:]*?:/', '', $MM_DB_DATABASE);
	$MM_DB_USERNAME = 'alicia';
	$MM_DB_PASSWORD = 'fatty';
	$MM_DB_LOCALE = 'En';
	$MM_DB_MSGLOCALE = 'En';
	$MM_DB_CTYPE = 'P';
	$KT_locale = $MM_DB_MSGLOCALE;
	$KT_dlocale = $MM_DB_LOCALE;
	$KT_serverFormat = '%Y-%m-%d %H:%M:%S';
	$QUB_Caching = 'false';

	$KT_localFormat = $KT_serverFormat;

	if (!defined('CONN_DIR')) define('CONN_DIR',dirname(__FILE__));
	//require_once(CONN_DIR.'/../adodb/adodb.inc.php');
	require_once(CONN_DIR.'/adodb/adodb.inc.php');
	$DB=&KTNewConnection($MM_DB_DBTYPE);

	if($MM_DB_DBTYPE == 'access' || $MM_DB_DBTYPE == 'odbc'){
		if($MM_DB_CTYPE == 'P'){
			$DB->PConnect($MM_DB_DATABASE, $MM_DB_USERNAME,$MM_DB_PASSWORD);
		} else $DB->Connect($MM_DB_DATABASE, $MM_DB_USERNAME,$MM_DB_PASSWORD);
	} else if (($MM_DB_DBTYPE == 'ibase') or ($MM_DB_DBTYPE == 'firebird')) {
		if($MM_DB_CTYPE == 'P'){
			$DB->PConnect($MM_DB_HOSTNAME.':'.$MM_DB_DATABASE,$MM_DB_USERNAME,$MM_DB_PASSWORD);
		} else $DB->Connect($MM_DB_HOSTNAME.':'.$MM_DB_DATABASE,$MM_DB_USERNAME,$MM_DB_PASSWORD);
	}else {
		if($MM_DB_CTYPE == 'P'){
			$DB->PConnect($MM_DB_HOSTNAME,$MM_DB_USERNAME,$MM_DB_PASSWORD, $MM_DB_DATABASE);
		} else $DB->Connect($MM_DB_HOSTNAME,$MM_DB_USERNAME,$MM_DB_PASSWORD, $MM_DB_DATABASE);
   }

	if (!function_exists('updateMagicQuotes')) {
		function updateMagicQuotes($HTTP_VARS){
			if (is_array($HTTP_VARS)) {
				foreach ($HTTP_VARS as $name=>$value) {
					if (!is_array($value)) {
						$HTTP_VARS[$name] = addslashes($value);
					} else {
						foreach ($value as $name1=>$value1) {
							if (!is_array($value1)) {
								$HTTP_VARS[$name1][$value1] = addslashes($value1);
							}
						}
					}
				}
			}
			return $HTTP_VARS;
		}

		if (!get_magic_quotes_gpc()) {
			$_GET = updateMagicQuotes($_GET);
			$_POST = updateMagicQuotes($_POST);
			$_COOKIE = updateMagicQuotes($_COOKIE);
		}

	}
	if (!isset($_SERVER['REQUEST_URI']) && isset($_ENV['REQUEST_URI'])) {
		$_SERVER['REQUEST_URI'] = $_ENV['REQUEST_URI'];
	}
	if (!isset($_SERVER['REQUEST_URI'])) {
		$_SERVER['REQUEST_URI'] = $_SERVER['PHP_SELF'].(isset($_SERVER['QUERY_STRING'])?"?".$_SERVER['QUERY_STRING']:"");
	}

	return($DB);
}

function getRatesCoolness($cat,$who,$when,$where){


	//Connection statement
	$DB=startDB();


	$sql = "Select carrier_id from carrier where carrier_name='2CONNECT'";
	$twoconn = $DB->SelectLimit($sql) or die($DB->ErrorMsg());

	switch($cat){
		case "CS":
			$supp = $who;
			$cust = $twoconn->Fields('carrier_id');
			break;
		case "CC":
			$supp = $twoconn->Fields('carrier_id');
			$cust = $who;
			break;
	}

	$when = GetSQLValueString($when,"date");
	$cust =	GetSQLValueString($cust,"int");
	$supp = GetSQLValueString($supp,"int");


	$sql = "select r.rate_id,r.master_id,m.calling_code,m.region_name,r.carrier_id,c1.carrier_name as supplier,c2.carrier_name as customer,r.rate,r.effective_date
			from rate as r, carrier as c1, carrier as c2, master as m
			where rate_id in
				(select rate_id from
				(select max(rate_id) as rate_id,master_id,carrier_id,customer_carrier_id,effective_date
				from rate
				where (master_id,carrier_id,customer_carrier_id,effective_date) in
					(select master_id,carrier_id,customer_carrier_id,max(effective_date)
					from rate where effective_date<=$when";

	switch($who){
		case 'ALL':
				if($cat == "CS"){
					$sql .= " and customer_carrier_id=$cust";
				}
				else {
					$sql .= " and carrier_id=$supp";
				}
				break;
		default:
				$sql .= " and carrier_id=$supp and customer_carrier_id=$cust";
	}

	switch($where){
		case 'ALL':
			break;
		default:
			$sql .= " and master_id in (select master_id from master where calling_code like '$where%')";
	}



	$sql .= " group by carrier_id,customer_carrier_id,master_id order by master_id,carrier_id,customer_carrier_id)
				group by master_id,carrier_id,customer_carrier_id,effective_date) as x)
					and m.master_id=r.master_id
					and c1.carrier_id=r.carrier_id
					and c2.carrier_id=r.customer_carrier_id
					order by calling_code asc,supplier asc";

	$rows =  pg_query($sql) or die('Query failed: ' . pg_last_error());
	return $rows;

}

function checkPassword($username,$password){

	//Connection statement
	$DB=startDB();

	$query = "SELECT * FROM liverate_login where username='$username' and password='$password'";
	$result  = $DB->SelectLimit($query) or die($DB->ErrorMsg());
	return ($result);
}

function getPartitionDetails(){


	//Connection statement
	$DB=startDB();

	$query = 	"select p.id,p.partition_name,c.carrier_name,pce.endpoint
				from partition as p, carrier as c, partition_carrier_endpoint as pce
				where
				pce.partition_id = p.id
				and c.carrier_id= pce.carrier_id
				and p.show = 1";

	$result  = pg_query($query) or die($DB->ErrorMsg());
	return ($result);
}

function fetchCDR($direction,$endpoint,$sdate,$edate,$limit,$error){

	//Connection statement
	$DB=startDB();

	$time_filter = "(start_time >='$sdate' AND start_time <= '$edate')";

	$endpoint_filter="";
	if (is_int(strpos($endpoint, ':'))) {
			$eps = explode(':',$endpoint); //multiple eps
			foreach($eps as $endpoint){
					 $ep = substr($endpoint,0,strlen($endpoint)- strlen(substr($endpoint,strpos($endpoint,"/"))));
					 $port = substr($endpoint,strpos($endpoint,"/") + 1);
					 $endpoint_filter = ($endpoint_filter == "")?" (EP = '$ep' AND PORT = $port)" : "$endpoint_filter OR (EP = '$ep' AND PORT = $port)";
			}
			$endpoint_filter = "($endpoint_filter)";

	} else {
			$ep = substr($endpoint,0,strlen($endpoint)- strlen(substr($endpoint,strpos($endpoint,"/"))));
			$port = substr($endpoint,strpos($endpoint,"/") + 1);
			$endpoint_filter = "((EP = '$ep' AND PORT = $port))";
	}


	switch($direction){
		case 'O':
			$endpoint_filter = str_replace('EP','call_dest_regid',$endpoint_filter);
			$endpoint_filter = str_replace('PORT','call_dest_uport',$endpoint_filter);
			break;
		case 'I':
			$endpoint_filter = str_replace('EP','call_source_regid',$endpoint_filter);
			$endpoint_filter = str_replace('PORT','call_source_uport',$endpoint_filter);
			break;
	}


	$error_filter = ($error=="")?" AND (call_error_int=0)":"";
	$order = " order by start_time desc";
	$limit_filter= " limit $limit";

	$query = "";
	$query = "SELECT start_time,hunting_attempts,ani,called_party_on_src,called_party_on_dst,clean_number,originator_ip,call_source_regid,call_source_uport,terminator_ip,call_dest_regid, call_dest_uport, call_duration, call_error_int,call_error_str,isdn_cause_code";
	$query .= " FROM report_new_cdr";
	$query .=" WHERE $time_filter";
	$query .=" AND $endpoint_filter";
	$query .=" $error_filter";
	$query .=" $order";
	$query .=" $limit_filter";

	
	#echo $query;
	#exit(0);
	$result  = pg_query($query) or die($DB->ErrorMsg());


	return ($result);
}

function fetchSummary($direction,$endpoint,$sdate,$edate){
	//Connection statement
	$DB=startDB();

	$time_filter = "(start_time >='$sdate' AND start_time <= '$edate')";

	$endpoint_filter="";
	if (is_int(strpos($endpoint, ':'))) {
			$eps = explode(':',$endpoint); //multiple eps
			foreach($eps as $endpoint){
					 $ep = substr($endpoint,0,strlen($endpoint)- strlen(substr($endpoint,strpos($endpoint,"/"))));
					 $port = substr($endpoint,strpos($endpoint,"/") + 1);
					 $endpoint_filter = ($endpoint_filter == "")?" (EP = '$ep' AND PORT = $port)" : "$endpoint_filter OR (EP = '$ep' AND PORT = $port)";
			}
			$endpoint_filter = "($endpoint_filter)";

	} else {
			$ep = substr($endpoint,0,strlen($endpoint)- strlen(substr($endpoint,strpos($endpoint,"/"))));
			$port = substr($endpoint,strpos($endpoint,"/") + 1);
			$endpoint_filter = "((EP = '$ep' AND PORT = $port))";
	}


	switch($direction){
		case 'O':
			$endpoint_filter = str_replace('EP','call_dest_regid',$endpoint_filter);
			$endpoint_filter = str_replace('PORT','call_dest_uport',$endpoint_filter);
			break;
		case 'I':
			$endpoint_filter = str_replace('EP','call_source_regid',$endpoint_filter);
			$endpoint_filter = str_replace('PORT','call_source_uport',$endpoint_filter);
			break;
	}

	$order = " order by start_time";

		
	$query = "";
	$query = "SELECT start_time, call_duration,call_error_int ";
	$query .= " FROM report_new_cdr";
	$query .=" WHERE $time_filter";
	$query .=" AND $endpoint_filter";
	$query .=" $order";
	
	#echo $query;
	#exit(0);
	$result  = pg_query($query) or die($DB->ErrorMsg());


	return ($result);
	
	

}
function getSourceCDRFileIds($max_file_id){
	//Connection statement
	$DB=startDB();
	
	$sql = "select id
			from ldr_files where 
			id > {$max_file_id}
			and lines > 0
			limit 20";
			
	$result  = pg_query($sql) or die($DB->ErrorMsg());
	
	return $result;
	
}

function getDestMaxCDRFileId(){
	//Connection statement
	$DB=startDB();
	$sql = "select max_cdr_file_id from control_report_new_cdr";
	$result  = pg_query($sql) or die($DB->ErrorMsg());
	return $result;
	
}

function getPopulateCriteria(){
	//Connection statement
	$DB=startDB();
	$sql = "select query_as, endpoint 
			from partition_carrier_endpoint 
			where query_as in ('C','S')";
	$result  = pg_query($sql) or die($DB->ErrorMsg());
	return $result;
	
}

function getCDRS($file_id){
	//Connection statement
	$DB=startDB();
	
	$sql = "SELECT start_time,hunting_attempts,ani,called_party_on_src,called_party_on_dst,clean_number,originator_ip,call_source_regid,call_source_uport,terminator_ip,call_dest_regid, call_dest_uport, call_duration, call_error_int,call_error_str,isdn_cause_code
			FROM new_cdr
			WHERE cdr_file_id = {$file_id}";
			
	$result  = pg_query($sql) or die($DB->ErrorMsg());
	
	return $result;
	
}

function update_control($file_id){
	//Connection statement
	$DB=startDB();
	$sql = "Update control_report_new_cdr set max_cdr_file_id={$file_id}";
	$result  = pg_query($sql) or die($DB->ErrorMsg());
	return $result;
	
}

function insert_report_new_cdr($line){
	
	//Connection statement
	$DB=startDB();

	#print_r($line);
	
	$cols ="";
	$vals = "";
	foreach($line as $key => $val){
		$cols .= "$key,";
		$vals .= "'$val',";
	}
	
	$cols = substr($cols,0,strlen($cols) - 1);
	$vals = substr($vals,0,strlen($vals) - 1);

		
	$sql = "Insert into report_new_cdr({$cols}) values({$vals});";
	#echo "\n$sql\n";
	#exit(0);
	$result  = pg_query($sql) or die($DB->ErrorMsg());
	
	

}

?>
