number_dct = {'38800000-38899999':'Batelco Mobile','16190000-16199999':'OLO Fixed','32200000-32299999':'Batelco Mobile','90000000-90009999':'Batelco Fixed','16500000-16599999':'2Connect Fixed','63660000-63669999':'Viva Mobile','66310000-66329999':'Zain Fixed','66330000-66399999':'Zain Mobile','16060000-16079999':'OLO Fixed','34350000-34599999':'Viva Mobile','69690000-69699999':'OLO Fixed','181-181':'Batelco Fixed','33000000-33999999':'Viva Mobile','32100000-32199999':'Batelco Mobile','80030000-80039999':'OLO Fixed','17000000-17999999':'Batelco Fixed','80080000-80089999':'2Connect Fixed','34300000-34349999':'Viva Mobile','66000000-66009999':'OLO Fixed','80888000-80888999':'OLO Fixed','39000000-39999999':'Batelco Mobile','34200000-34299999':'Viva Mobile','95150000-95159999':'2Connect Fixed','16100000-16109999':'OLO Fixed','95050000-95059999':'2Connect Fixed','16000000-16039999':'OLO Fixed','84480000-84489999':'2Connect Fixed','63330000-63339999':'Viva Mobile','13110000-13119999':'Viva Fixed','13300000-13399999':'OLO Fixed','87700000-87700999':'Viva Fixed','69660000-69669999':'OLO Fixed','66700000-66769999':'Batelco Mobile','38700000-38799999':'Batelco Mobile','87000000-87000999':'Viva Fixed','80020000-80029999':'OLO Fixed','90010000-90010999':'OLO Fixed','37000000-37999999':'Zain Mobile','63610000-63619999':'Viva Mobile','35000000-35199999':'Viva Mobile','34000000-34199999':'Viva Mobile','35500000-35599999':'Viva Mobile','77000000-77999999':'OLO Fixed','80000000-80009999':'Batelco Fixed','90090000-90099999':'OLO Fixed','141-141':'Batelco Fixed','35600000-35699999':'Viva Mobile','69960000-69969999':'OLO Fixed','87780000-87789999':'2Connect Fixed','80090000-80099999':'OLO Fixed','80100000-80100999':'Viva Fixed','32000000-32099999':'Batelco Mobile','63000000-63009999':'Viva Mobile','38000000-38499999':'Batelco Mobile','35300000-35399999':'Viva Mobile','35400000-35499999':'Viva Mobile','140-140':'Batelco Fixed','13600000-13699999':'Zain Fixed','80010000-80019999':'OLO Fixed','80408000-80408999':'OLO Fixed','69990000-69999999':'OLO Fixed','66900000-66999999':'Zain Mobile','80800000-80809999':'OLO Fixed','35900000-35999999':'Viva Mobile','32300000-32399999':'Batelco Mobile','38900000-38999999':'Batelco Mobile','16600000-16699999':'OLO Fixed','13100000-13109999':'Viva Fixed','13120000-13299999':'OLO Fixed','65000000-65009999':'OLO Fixed','66600000-66699999':'Zain Mobile','16160000-16169999':'OLO Fixed','80070000-80079999':'Zain Fixed','61110000-61119999':'OLO Fixed','71110000-71119999':'OLO Fixed','66770000-66799999':'Batelco Fixed','66300000-66309999':'Zain Mobile','36000000-36999999':'Zain Mobile','80060000-80069999':'OLO Fixed'}

number_range  = {'Batelco Mobile':{'num_range':[]},
                 'Batelco Fixed':{'num_range':[]},
                 'Viva Mobile':{'num_range':[]},
                 'Viva Fixed':{'num_range':[]},
                 'Zain Fixed':{'num_range':[]},
                 'Zain Mobile':{'num_range':[]},
                 '2Connect Mobile':{'num_range':[]},
                 '2Connect Fixed':{'num_range':[]},
                 'OLO Mobile':{'num_range':[]},
                 'OLO Fixed':{'num_range':[]},
                }
for key,value in number_dct.items():
    print key, value
    number_range[value]['num_range'].append(key)
    
print number_range
