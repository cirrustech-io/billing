"""
import csv
top_ten_lst = [{'country': u'Algeria', 'code': u'00213', 'call_data': 12512}, {'country': u'Argentina', 'code': u'0054', 'call_data': 536}, {'country': u'Ascension', 'code': u'00247', 'call_data': 34}, {'country': u'Australia', 'code': u'0061', 'call_data': 114757}, {'country': u'Austria', 'code': u'0043', 'call_data': 46032}, {'country': u'Azerbaijan', 'code': u'00994', 'call_data': 2777}, {'country': u'Bahamas', 'code': u'001242', 'call_data': 237}, {'country': u'Bangladesh', 'code': u'00880', 'call_data': 287952}]

top_lst = []
for top in top_ten_lst:
        top_lst.append([top['country'], top['code'], top['call_data']])
        top_header = ['Country', 'Code', 'Call seconds']

with open('top_inbound.csv', 'wb') as csvfile:
    inbound_writer = csv.writer(csvfile)
    inbound_writer.writerow(top_header)
    inbound_writer.writerows(top_lst)

"""

def switch(x):
    return { 
       1<x<4: 'several',
       5<x<40: 'lots'
    }[1]

def main(x):
    print switch(x)
    return 0

main(3)
