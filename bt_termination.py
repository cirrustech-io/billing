import csv
from billing_config import number_range_lookup, rate_dict, output_dir, file_path
from billing_helper import get_csv_writer, write_to_csv, get_duration, close_csv
from datetime import datetime
import math
from log_generator import log_writer, syslog_writer

app_name = 'bt_termination'
log_file = file_path + 'logs/'+app_name+'.log'
logger = log_writer(log_file, app_name)

def get_number_range(filename):
   with open(filename, 'rb') as range_file:
       range_reader = csv.reader(range_file, delimiter=',', quotechar='|')
       range_dct = {}
       for row in range_reader:
           if row[-1]:
               range_dct[row[0]+'<=num<'+row[1]] = row[-1]
       return range_dct

def get_bt_rate(filename):
   with open(filename, 'rb') as rate_file:
       rate_reader = csv.reader(rate_file, delimiter=',', quotechar='|')
       rate_dct = {}
       for row in rate_reader:
           rate_dct[row[0].strip()+'-'+row[2].strip()] = {'destination':row[2].strip(), 'rate':row[3].strip(), 'src':row[0].strip()}
       return rate_dct

def get_src(num):
    if len(str(num))==8:
        try:
            return number_range_lookup(num)
        except Exception,e:
            ### Exception handling for 8 Digit International numbers
            return "International"
    else:
        return "International"

def generate_bt_report(filename):
    logger.info("Execution Start Time : %s" % datetime.now())

    with open(filename, 'rb') as web_file:

       web_reader = csv.reader(web_file)#, delimiter=',', quotechar='|')
       range_dct = {}
       tot_count = 1
       tot_sec = 0.0
       rem_sec = 0.0
       tot_charge = 0.0
       tot_min = 0.0
       report_month = (filename.split('_')[2]+'_'+filename.split('_')[3].split('.')[0]).title()

       ###Create BT IDV Termination Detailed Report 
       bt_idv_file = output_dir+'BT_IDV_'+report_month+'.csv'
       bt_idv_headers = ['Date/Time','Source','Tollfree','Duration','Charges']
       bt_idv_writer, bt_fd = get_csv_writer(bt_idv_file, bt_idv_headers)

       src_dct = rate_dict.copy()
       for row in web_reader:
           try:
               if not row[2]:
                   row[2] = '0'
               ani_num = int(row[2])

               src_carrier = get_src(ani_num)
               toll_free = row[17].replace('973','')

               if(toll_free.startswith('974')):
                   toll_free = '800'
           
               if(src_carrier and float(row[5])!=0):
                   rate_key = src_carrier+'-'+toll_free[:3]
                   call_dest = rate_dict[rate_key]['destination']
                   call_rate = float(rate_dict[rate_key]['rate'])
                   sec = math.ceil(float(row[5]))
                   minute = sec/60
                   src = rate_dict[rate_key]['src']
                   duration = get_duration(sec)
                   charge = minute * call_rate 
                   out_row = [row[0], src, toll_free, duration, round(charge, 6)]
                   write_to_csv(bt_idv_writer, out_row)

                   src_row = out_row
                   src_row.append(minute)
                   src_dct[rate_key]['src_out'].append(src_row)
                   src_dct[rate_key]['minute'] = sum([row[5] for row in src_dct[rate_key]['src_out']])
                   src_dct[rate_key]['charge'] = sum([row[4] for row in src_dct[rate_key]['src_out']])

                   ###Calculate total charge, total count, total second, total minute
                   tot_charge+=charge
                   tot_count+=1
                   tot_sec+=sec
                   tot_min+=minute

           except Exception,e:
               err_message = 'Wrong ANI - %s, TollFree - %s, Data - %s' % (row[2], row[17], row)
               logger.error(err_message)
               syslog_writer(app_name, "ERROR", err_message)
               try:
                   rem_sec+=float(row[5])
               except:
                   pass

       ### Write Total Charge to the Detailed Report
       write_to_csv(bt_idv_writer, ['','','','','$%.2f' % tot_charge])
       close_csv(bt_fd)
       logger.info("Completed writing into %s" % (bt_idv_file))
       syslog_writer(app_name, "INFO", "Completed writing into %s" % (bt_idv_file))

       ### Create BT IDV Termination - Vendor based Report
       bt_src_file = output_dir+'British_Telecom_Indirect_Voice_Service_'+report_month+'.csv'
       bt_src_headers = ['Source','Destination','Calls','Minutes','Rate','Charges']
       bt_src_writer, bt_src_fd = get_csv_writer(bt_src_file, bt_src_headers)

       src_charge = 0.0
       src_min = 0.0
       src_call = 0

       for key,value in src_dct.items():
           if(src_dct[key]['minute']):
               no_of_call = len(src_dct[key]['src_out'])
               bt_src_out = [src_dct[key]['src'], src_dct[key]['destination'], no_of_call, src_dct[key]['minute'], '$'+str(src_dct[key]['rate']), '$'+str(src_dct[key]['charge'])]
               write_to_csv(bt_src_writer, bt_src_out)
               src_charge += src_dct[key]['charge']
               src_min += src_dct[key]['minute']
               src_call += no_of_call

       write_to_csv(bt_src_writer, ['Total','',src_call,round(src_min,2),'','$'+str(round(src_charge, 2))])
       close_csv(bt_src_fd)
       logger.info("Completed writing into %s" % (bt_src_file))
       syslog_writer(app_name, "INFO", "Completed writing into %s" % (bt_src_file))

       logger.info("Execution End Time : %s" % datetime.now())


if __name__ == '__main__':
  
    import sys
    file_name = sys.argv[1]

    #get_number_range('./docs/TRA_Number_Range_Refined.csv')
    #print get_bt_rate('./docs/British_Telecom_Rates_2016.csv')
    generate_bt_report(file_name)
    #print get_src(37300525)
    
