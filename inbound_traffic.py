#
# Small script to show PostgreSQL and Pyscopg together
#

import psycopg2
import csv
import json
import pprint

def fetch_data(query):

    try:
        conn = psycopg2.connect("dbname='billing' user='haifa' host='80.88.242.6' password='7c2d3ab9'")
        cur = conn.cursor()
        cur.execute(query)
        rows = cur.fetchall()
        return rows
    except:
        return "Unable to connect to the database"

def convert_sec_to_min(sec):
    minutes = sec/60
    return minutes 

def query_generation_outbound():
    #sec_data = fetch_data("SELECT SUM(call_duration_fractional) FROM new_cdr WHERE (called_party_on_dst ILIKE '%123#1%' OR called_party_on_dst ILIKE '%568#%') AND start_time >= '"+start_date+"' AND start_time < '"+end_date+"';");
    sec_data = fetch_data("select ani from new_cdr where called_party_on_src ILIKE '973165%' and (length(ani) > 8 and ani not ilike '973%')  and call_duration_fractional >0.0 and start_time>= '2016-04-01' and start_time < '2016-05-01';");
    import csv

    with open('ani.csv', 'wb') as csvfile:
        ani_writer = csv.writer(csvfile)
        ani_writer.writerows(sec_data)
    #min_data = convert_sec_to_min(sec_data[0][0])

def top_ten_destinations(call_type, start_date, end_date, country_code, country_name):
    country_code_with_zero = country_code.replace('+','00')
    country_code_without_zero = country_code.replace('+','')
    
    call_data = fetch_data("SELECT SUM(call_duration_fractional)/2 FROM new_cdr WHERE (ani ILIKE '"+country_code_with_zero+"%' OR ani ILIKE '%"+country_code_without_zero+"%') AND start_time >= '"+start_date+"' AND start_time <= '"+end_date+"';")
    return call_data
    
 

if __name__ == '__main__':
    
    import sys
    #start_date = sys.argv[1]
    #end_date = sys.argv[2]
    #print start_date
    #print end_date
    
    with open('inter_codes.json') as json_data:
        data_lst = []
        data = json.load(json_data)
        for item in data['countries']:
            data_dct = {}
            code = item['code'].replace(' ','').strip()
            country = item['name'].strip()
            call_data = top_ten_destinations('INBOUND', '2015-05-01', '2016-05-01', code, country)
            if (call_data[0][0]):
                data_dct['code'] = code.replace('+','00')
                data_dct['country'] = country
                data_dct['call_seconds'] = int(round(call_data[0][0]))
                data_lst.append(data_dct)
    
    top_all_lst = sorted(data_lst, key=lambda x: -x['call_seconds'])[0:11]
    top_ten_lst = top_all_lst[0:12]

    
    top_lst = []
    for top in top_ten_lst:
        top_header = ['Country', 'Code', 'Call seconds']
        top_lst.append([top['country'], top['code'], top['call_seconds']])
        
    with open('top_inbound.csv', 'wb') as csvfile:
        inbound_writer = csv.writer(csvfile)
        inbound_writer.writerow(top_header)
        inbound_writer.writerows(top_lst)
    


        
