
def number_range_lookup(num):
    return {38800000<=num<38899999:'Batelco Mobile',16190000<=num<16199999:'OLO Fixed',32200000<=num<32299999:'Batelco Mobile',90000000<=num<90009999:'Batelco Fixed',16500000<=num<16599999:'2Connect Fixed',63660000<=num<63669999:'Viva Mobile',66310000<=num<66329999:'Zain Fixed',66330000<=num<66399999:'Zain Mobile',16060000<=num<16079999:'OLO Fixed',34350000<=num<34599999:'Viva Mobile',69690000<=num<69699999:'OLO Fixed',181<=num<181:'Batelco Fixed',33000000<=num<33999999:'Viva Mobile',32100000<=num<32199999:'Batelco Mobile',80030000<=num<80039999:'OLO Fixed',17000000<=num<17999999:'Batelco Fixed',80080000<=num<80089999:'2Connect Fixed',34300000<=num<34349999:'Viva Mobile',66000000<=num<66009999:'OLO Fixed',80888000<=num<80888999:'OLO Fixed',39000000<=num<39999999:'Batelco Mobile',34200000<=num<34299999:'Viva Mobile',95150000<=num<95159999:'2Connect Fixed',16100000<=num<16109999:'OLO Fixed',95050000<=num<95059999:'2Connect Fixed',16000000<=num<16039999:'OLO Fixed',84480000<=num<84489999:'2Connect Fixed',63330000<=num<63339999:'Viva Mobile',13110000<=num<13119999:'Viva Fixed',13300000<=num<13399999:'OLO Fixed',87700000<=num<87700999:'Viva Fixed',69660000<=num<69669999:'OLO Fixed',66700000<=num<66769999:'Batelco Mobile',38700000<=num<38799999:'Batelco Mobile',87000000<=num<87000999:'Viva Fixed',80020000<=num<80029999:'OLO Fixed',90010000<=num<90010999:'OLO Fixed',37000000<=num<37999999:'Zain Mobile',63610000<=num<63619999:'Viva Mobile',35000000<=num<35199999:'Viva Mobile',34000000<=num<34199999:'Viva Mobile',35500000<=num<35599999:'Viva Mobile',77000000<=num<77999999:'OLO Fixed',80000000<=num<80009999:'Batelco Fixed',90090000<=num<90099999:'OLO Fixed',141<=num<141:'Batelco Fixed',35600000<=num<35699999:'Viva Mobile',69960000<=num<69969999:'OLO Fixed',87780000<=num<87789999:'2Connect Fixed',80090000<=num<80099999:'OLO Fixed',80100000<=num<80100999:'Viva Fixed',32000000<=num<32099999:'Batelco Mobile',63000000<=num<63009999:'Viva Mobile',38000000<=num<38499999:'Batelco Mobile',35300000<=num<35399999:'Viva Mobile',35400000<=num<35499999:'Viva Mobile',140<=num<140:'Batelco Fixed',13600000<=num<13699999:'Zain Fixed',80010000<=num<80019999:'OLO Fixed',80408000<=num<80408999:'OLO Fixed',69990000<=num<69999999:'OLO Fixed',66900000<=num<66999999:'Zain Mobile',80800000<=num<80809999:'OLO Fixed',35900000<=num<35999999:'Viva Mobile',32300000<=num<32399999:'Batelco Mobile',38900000<=num<38999999:'Batelco Mobile',16600000<=num<16699999:'OLO Fixed',13100000<=num<13109999:'Viva Fixed',13120000<=num<13299999:'OLO Fixed',65000000<=num<65009999:'OLO Fixed',66600000<=num<66699999:'Zain Mobile',16160000<=num<16169999:'OLO Fixed',80070000<=num<80079999:'Zain Fixed',61110000<=num<61119999:'OLO Fixed',71110000<=num<71119999:'OLO Fixed',66770000<=num<66799999:'Batelco Fixed',66300000<=num<66309999:'Zain Mobile',36000000<=num<36999999:'Zain Mobile',80060000<=num<80069999:'OLO Fixed'}[1]

number_range = {
                'Batelco Fixed': {'num_range': ['90000000-90009999', '140-140', '80000000-80009999', '181-181', '17000000-17999999', '66770000-66799999', '141-141']}, 
                'Batelco Mobile': {'num_range': ['38900000-38999999', '32200000-32299999', '32300000-32399999', '66700000-66769999', '38700000-38799999', '39000000-39999999', '38000000-38499999', '32100000-32199999', '32000000-32099999', '38800000-38899999']}, 
                'Zain Fixed': {'num_range': ['13600000-13699999', '66310000-66329999', '80070000-80079999']},
                'Zain Mobile': {'num_range': ['66300000-66309999', '66600000-66699999', '66900000-66999999', '36000000-36999999', '66330000-66399999', '37000000-37999999']}, 
                'Viva Fixed': {'num_range': ['87700000-87700999', '13100000-13109999', '87000000-87000999', '80100000-80100999', '13110000-13119999']}, 
                'Viva Mobile': {'num_range': ['34000000-34199999', '35000000-35199999', '34200000-34299999', '34300000-34349999', '35400000-35499999', '63660000-63669999', '35300000-35399999', '33000000-33999999', '63000000-63009999', '35900000-35999999', '63610000-63619999', '35500000-35599999', '63330000-63339999', '35600000-35699999', '34350000-34599999']}, 
                'OLO Fixed': {'num_range': ['13120000-13299999', '69690000-69699999', '80030000-80039999', '69660000-69669999', '80800000-80809999', '80020000-80029999', '90090000-90099999', '16060000-16079999', '13300000-13399999', '61110000-61119999', '77000000-77999999', '80408000-80408999', '16160000-16169999', '16190000-16199999', '90010000-90010999', '66000000-66009999', '71110000-71119999', '80010000-80019999', '16600000-16699999', '69960000-69969999', '69990000-69999999', '80888000-80888999', '16000000-16039999', '65000000-65009999', '80060000-80069999', '80090000-80099999', '16100000-16109999']},
                'OLO Mobile': {'num_range': []}, 
                '2Connect Fixed': {'num_range': ['95150000-95159999', '87780000-87789999', '84480000-84489999', '16500000-16599999', '95050000-95059999', '80080000-80089999']}, 
                '2Connect Mobile': {'num_range': []}, 
                }

rate_dict = {
             'Batelco Mobile-165': {'src': 'Batelco Mobile', 'rate': '0.02', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             'Batelco Mobile-800': {'src': 'Batelco Mobile', 'rate': '0.041', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             'Batelco Fixed-165': {'src': 'Batelco Fixed', 'rate': '0.02', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             'Batelco Fixed-800': {'src': 'Batelco Fixed', 'rate': '0.0275', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             'Viva Mobile-165': {'src': 'Viva Mobile', 'rate': '0.02', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             'Viva Mobile-800': {'src': 'Viva Mobile', 'rate': '0.0465', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             'Viva Fixed-165': {'src': 'Viva Fixed', 'rate': '0.02', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             'Viva Fixed-800': {'src': 'Viva Fixed', 'rate': '0.033', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'},
             'Zain Mobile-165': {'src': 'Zain Mobile', 'rate': '0.02', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             'Zain Mobile-800': {'src': 'Zain Mobile', 'rate': '0.0465', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             'Zain Fixed-165': {'src': 'Zain Fixed', 'rate': '0.02', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             'Zain Fixed-800': {'src': 'Zain Fixed', 'rate': '0.033', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             'OLO Mobile-165': {'src': 'OLO Mobile', 'rate': '0.02', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             'OLO Mobile-800': {'src': 'OLO Mobile', 'rate': '0.0465', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             'OLO Fixed-165': {'src': 'OLO Fixed', 'rate': '0.02', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             'OLO Fixed-800': {'src': 'OLO Fixed', 'rate': '0.033', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             '2Connect Fixed-165': {'src': '2Connect Fixed', 'rate': '0.02', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             '2Connect Fixed-800': {'src': '2Connect Fixed', 'rate': '0.02', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             'International-165': {'src': 'International', 'rate': '0.02', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
             'International-800': {'src': 'International', 'rate': '0.02', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'USD'}, 
            }

infonas_term_rate_dict = {
             'Batelco Mobile-165': {'src': 'Batelco Mobile', 'rate': '0.00461', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'Batelco Mobile-800': {'src': 'Batelco Mobile', 'rate': '0.02', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'Batelco Fixed-165': {'src': 'Batelco Fixed', 'rate': '0.00461', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'Batelco Fixed-800': {'src': 'Batelco Fixed', 'rate': '0.01', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'Viva Mobile-165': {'src': 'Viva Mobile', 'rate': '0.00461', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'Viva Mobile-800': {'src': 'Viva Mobile', 'rate': '0.02', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'Viva Fixed-165': {'src': 'Viva Fixed', 'rate': '0.00461', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'Viva Fixed-800': {'src': 'Viva Fixed', 'rate': '0.01', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'Zain Mobile-165': {'src': 'Zain Mobile', 'rate': '0.00461', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'Zain Mobile-800': {'src': 'Zain Mobile', 'rate': '0.02', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'Zain Fixed-165': {'src': 'Zain Fixed', 'rate': '0.00461', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'Zain Fixed-800': {'src': 'Zain Fixed', 'rate': '0.01', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'OLO Mobile-165': {'src': 'OLO Mobile', 'rate': '0.00461', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'OLO Mobile-800': {'src': 'OLO Mobile', 'rate': '0.02', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'OLO Fixed-165': {'src': 'OLO Fixed', 'rate': '0.00461', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'OLO Fixed-800': {'src': 'OLO Fixed', 'rate': '0.01', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             '2Connect Fixed-165': {'src': '2Connect Fixed', 'rate': '0.00461', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             '2Connect Fixed-800': {'src': '2Connect Fixed', 'rate': '0.01', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'International-165': {'src': 'International', 'rate': '0.00995', 'destination': '165','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
             'International-800': {'src': 'International', 'rate': '0.01', 'destination': '8008','src_out':[], 'minute':'', 'charge':'', 'currency':'BHD'},
            }


O2_rate_dict = {
                '2Connect Fixed':{'rate':'0.05', 'minute':0.0, 'charge':0.0},
                'Batelco Fixed':{'rate':'0.05', 'minute':0.0, 'charge':0.0},
                'Zain Fixed':{'rate':'0.05', 'minute':0.0, 'charge':0.0},
                'Viva Fixed':{'rate':'0.05', 'minute':0.0, 'charge':0.0},
                'OLO Fixed':{'rate':'0.05', 'minute':0.0, 'charge':0.0},
                'Batelco Mobile':{'rate':'0.06', 'minute':0.0, 'charge':0.0},
                'Zain Mobile':{'rate':'0.06', 'minute':0.0, 'charge':0.0},
                'Viva Mobile':{'rate':'0.06', 'minute':0.0, 'charge':0.0},
               }

file_path = '/home/binny/billing/'

### Output files
output_dir = file_path + 'docs/BT_Termination/'
output_pbx_dir = file_path + 'docs/Zero_Ani/'
output_tra_dir = file_path + 'docs/tra_report/'

### Mail Settings
smtp_host = 'mail.i.infonas.com'
smtp_port = 25
sender = 'noreply@i.infonas.com'
pbx_receiver = ['binny.abraham@infonas.com', 'darshay.pathak@infonas.com', 'noc@infonas.com']

### DB Settings
db_name='billing'
db_user='haifa'
db_host='80.88.242.6'
db_password='7c2d3ab9'

