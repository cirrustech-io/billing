#
# Small script to show PostgreSQL and Pyscopg together
#

import psycopg2
import csv
import json
from billing_helper import get_csv_writer, write_rows_to_csv, close_csv
from datetime import datetime

def exec_query(query, exec_type):

    try:
        print query
        conn = psycopg2.connect("dbname='billing' user='haifa' host='80.88.242.6' password='7c2d3ab9'")
        cur = conn.cursor()
        cur.execute(query)
        if(exec_type == 'select'):
            rows = cur.fetchall()
        conn.commit()
        cur.close()
	conn.close()
        if(exec_type == 'select'):
            return rows
        else:
            return "Executed the query : "+query
    except:
        return "Unable to connect to the database"

def prepare_query(query, exec_type):
    return exec_query(query, exec_type)


if __name__ == '__main__':
    
    import sys
    file_name = sys.argv[1]
    csv_type = sys.argv[2]

    if csv_type == 'read':
        query_list = open(file_name, 'r').readlines()
        for query in query_list:
            print prepare_query(query, 'insert')
    else:
        start_date = sys.argv[3]
        end_date = sys.argv[4]
        req_type = sys.argv[5].lower()
        out_month = datetime.strptime(start_date, "%Y-%m-%d").strftime("%d_%b_%Y")
        out_file = file_name+'/'+out_month+'_'+req_type+'.csv'
        out_header = ['cdr_file_id', 'cdr_file_line', 'ani', 'clean_number', 'call_seconds', 'minutes', 'start_date', 'call_source_reg_id', 'call_source_uport', 'call_dest_regid', 'call_dest_uport']


        if req_type == 'batelco_fixed':
	    ###Batelco Fixed
            query =  "SELECT cdr_file_id, cdr_file_line, ani, clean_number, call_duration_int, call_duration_fractional/60, start_time, call_source_regid, call_source_uport, call_dest_regid, call_dest_uport FROM new_cdr WHERE originator_ip in ('80.88.247.115','80.88.247.135','80.88.247.12') AND start_time >= '"+start_date+"' AND start_time < '"+end_date+"' AND call_duration_fractional > 0 AND (ani ~ '^17[0-9]' OR ani ~ '^667[0-9]') AND (clean_number LIKE '8008%' or clean_number LIKE '9738008%') ORDER BY start_time;"

        if req_type == 'batelco_mobile':
            ###Batelco Mobile
            query = "SELECT cdr_file_id, cdr_file_line, ani, clean_number, call_duration_int, call_duration_fractional/60, start_time, call_source_regid, call_source_uport, call_dest_regid, call_dest_uport FROM new_cdr WHERE originator_ip in ('80.88.247.115','80.88.247.135','80.88.247.12') AND start_time >= '"+start_date+"' AND start_time < '"+end_date+"' AND call_duration_fractional > 0 AND (ani ~ '^388[0-9]' OR ani ~ '^322[0-9]' OR ani ~ '^321[0-9]' OR ani ~ '^39[0-9]' OR ani ~ '^387[0-9]' OR ani ~ '^320[0-9]' OR ani ~ '^323[0-9]' OR ani ~ '^389[0-9]' OR ani ~ '^667[0-6][0-9]' OR ani ~ '^38[0-4][0-9]') AND (clean_number LIKE '8008%' or clean_number LIKE '9738008%') ORDER BY start_time;"
        
        if req_type == 'olo_fixed':
            ###OLO Fixed
            query = "SELECT cdr_file_id, cdr_file_line, ani, clean_number, call_duration_int, call_duration_fractional/60, start_time, call_source_regid, call_source_uport, call_dest_regid, call_dest_uport FROM new_cdr WHERE originator_ip in ('80.88.247.115','80.88.247.135','80.88.247.12') AND start_time >= '"+start_date+"' AND start_time < '"+end_date+"' AND call_duration_fractional > 0 AND (ani ~ '^136[0-9]' OR ani ~ '^663[1-2][0-9]' OR ani ~ '^8007[0-9]' OR ani ~ '^87700[0-9]' OR ani ~ '^1310[0-9]' OR ani ~ '^87000[0-9]' OR ani ~ '^80100[0-9]' OR ani ~ '^1311[0-9]' OR ani ~ '^13[1-2]2[0-9]' OR ani ~ '^6969[0-9]' OR ani ~ '^8003[0-9]' OR ani ~ '^6966[0-9]' OR ani ~ '^8080[0-9]' OR ani ~ '^8002[0-9]' OR ani ~ '^9009[0-9]' OR ani ~ '^160[6-7[0-9]' OR ani ~ '^133[0-9]' OR ani ~ '^6111[0-9]' OR ani ~ '^77[0-9]' OR ani ~ '^80408[0-9]' OR ani ~ '^1616[0-9]' OR ani ~ '^1619[0-9]' OR ani ~ '^90010[0-9]' OR ani ~ '^6600[0-9]' OR ani ~ '^7111[0-9]' OR ani ~ '^8001[0-9]' OR ani ~ '^166[0-9]' OR ani ~ '^6996[0-9]' OR ani ~ '^6999[0-9]' OR ani ~ '^80888[0-9]' OR ani ~ '^160[0-3][0-9]' OR ani ~ '^6500[0-9]' OR ani ~ '^8006[0-9]' OR ani ~ '^8009[0-9]' OR ani ~ '^1610[0-9]') AND (clean_number LIKE '8008%' or clean_number LIKE '9738008%') ORDER BY start_time;"

        if req_type == 'olo_mobile':
            ###OLO Mobile
            query = "SELECT cdr_file_id, cdr_file_line, ani, clean_number, call_duration_int, call_duration_fractional/60, start_time, call_source_regid, call_source_uport, call_dest_regid, call_dest_uport FROM new_cdr WHERE originator_ip in ('80.88.247.115','80.88.247.135','80.88.247.12') AND start_time >= '"+start_date+"' AND start_time < '"+end_date+"' AND call_duration_fractional > 0 AND (ani ~ '^6630[0-9]' OR ani ~ '^666[0-9]' OR ani ~ '^669[0-9]' OR ani ~ '^36[0-9]' OR ani ~ '^663[3-9][0-9]' OR ani ~ '^37[0-9]' OR ani ~ '^34[0-1][0-9]' OR ani ~ '^35[0-1][0-9]' OR ani ~ '^342[0-9]' OR ani ~ '^343[0-4][0-9]' OR ani ~ '^354[0-9]' OR ani ~ '^6366[0-9]' OR ani ~ '^353[0-9]' OR ani ~ '^33[0-9]' OR ani ~ '^6300[0-9]' OR ani ~ '^359[0-9]' OR ani ~ '^6361[0-9]' OR ani ~ '^6333[0-9]' OR ani ~ '^356[0-9]' OR ani ~ '^34[3-5][5-9][0-9]' OR ani ~ '^343[0-4][0-9]' ) AND (clean_number LIKE '8008%' or clean_number LIKE '9738008%') ORDER BY start_time;"

        if req_type == 'cps':
            ### Batelco CPS 
            query = "select cdr_file_id, cdr_file_line, ani, clean_number, call_duration_int, call_duration_fractional/60, start_time, call_source_regid, call_source_uport, call_dest_regid, call_dest_uport from new_cdr where start_time >= '"+start_date+"' and start_time < '"+end_date+"' and call_duration_fractional> 0.0 and (call_source_regid = '2CONNECT_AS5350B' OR call_source_regid = '2CONNECT_AS5400A') AND call_source_uport IN (200, 201, 202, 203, 204, 205, 206, 207, 208, 210, 211, 214, 215, 216, 217, 218, 219, 222, 223, 224, 225, 226, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 249, 250, 251, 252, 254) ORDER BY start_time;"

        ### FCC
        if req_type == 'fcc_international':
            # International
            query = "SELECT SUM(call_duration_fractional/60), SUM(call_duration_fractional/60)*0.00498 FROM new_cdr WHERE start_time>='%s' AND start_time<'%s' AND call_dest_regid = '%s' and length(ani)!=%s;" % (start_date, end_date, 'FREE_CONF_CALL', 8)
            out_header = ['Minutes', 'Amount']

        if req_type == 'fcc_national':
            # National
            query = "SELECT SUM(call_duration_fractional/60), SUM(call_duration_fractional/60)*0.00231 FROM new_cdr WHERE start_time>='%s' AND start_time<'%s' AND call_dest_regid = '%s' and length(ani)=%s;" % (start_date, end_date, 'FREE_CONF_CALL', 8)
            out_header = ['Minutes', 'Amount']
        
        
        csv_writer, out_fd = get_csv_writer(out_file, out_header)
        data = prepare_query(query, 'select')
        print data
        write_rows_to_csv(csv_writer, data)
        close_csv(out_fd)


